import spock.lang.*

class TheFunctionTest extends Specification {
    TheFunction theFunction = new TheFunction()

    def "test count 1"() {
        when:
        int result = theFunction.count(0, 0)

        then:
        result == 1
    }
    def "test count 2"() {
        when:
        int result = theFunction.count(1, 1)

        then:
        result == 1
    }
    def "test count 3"() {
        when:
        int result = theFunction.count(3, 2)

        then:
        result == 12
    }
    def "test count 4"() {
        when:
        int result = theFunction.count(0, -1)

        then:
        thrown(Exception)
    }

}
