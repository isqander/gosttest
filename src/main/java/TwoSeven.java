import java.util.stream.IntStream;

public class TwoSeven {

    public static void main(String[] args) {
        IntStream.range(1, 101).mapToObj(i -> {
            int a = i % 2;
            int b = i % 7;
            if (a + b == 0) {
                return "TwoSeven";
            } else if (a == 0) {
                return "Two";
            } else if (b == 0) {
                return "Seven";
            } else return i;
        }).forEach(System.out::println);
    }
}
