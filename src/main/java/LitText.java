import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LitText {

    public static void main(String[] args) {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")){
            System.out.println("Введите текст. После ввода нажмите Ctrl+Z");
        } else {
            System.out.println("Введите текст. После ввода нажмите Ctrl+D");
        }

        List<String> words = new BufferedReader(
                new InputStreamReader(System.in, StandardCharsets.UTF_8)).lines()
                .flatMap(Pattern.compile("[^\\p{L}]+")::splitAsStream)
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
        Map<String, Integer> countWords = new HashMap<>();
        for (String value : words) {
            int count = countWords.containsKey(value) ? countWords.get(value) : 0;
            countWords.put(value, count + 1);
        }

        Comparator<Map.Entry<String, Integer>> comparator = Map.Entry.comparingByValue();
        countWords.entrySet().stream().sorted(comparator.reversed()).forEach(System.out::println);
    }
}
