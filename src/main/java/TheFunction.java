public class TheFunction {
    int count(int m, int r) throws Exception {
        if (m < 0 || r < 0 || m < r) {
            throw new Exception("Wrong input");
        }
        //конечно можно оптимизировать, но так структурнее и нагляднее
        return factorial(m) * factorial(r) * factorial(m - r);
    }

    int factorial(int a) {
        int factorial = 1;
        for (int i = 1; i <= a; i++) {
            factorial *= i;
        }
        return factorial;
    }
}
